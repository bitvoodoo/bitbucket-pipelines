
This needs to be a public repository, for our internal build plans

The following variables neds to be set as [Workspace variable](https://bitbucket.org/bitvoodoo/workspace/settings/addon/admin/pipelines/account-variables
).

Variable | Less
--- | --- 
`NPM_USER` | The authentication (base 64) to authenticate against the Repository| 
`MAVEN_USERNAME` | The username to download artifacts from the internal maven repositories and to deploy new artifacts| 
`MAVEN_PASSWORD` | The password to download artifacts from the internal maven repositories and to deploy new artifacts| 
`BITBUCKET_USERNAME` | The username to authenticate against bitbucket Cloud (to commit changes or upload artifacts)| 
`BITBUCKET_USER_MAIL` | The email to set the correct git user|
`BITBUCKET_APP_TOKEN` | The app token for the `BITBUCKET_USER`| 


# Testing Scripts

## provide-app-version.sh

To test the ```includes/provide-app-version.sh``` script: 

- Activate the test mode ```export test=1```
- Go into the ```tests``` folder
- Set master and develop branch ```export MASTER_BRANCH=master``` and ```export DEVELOP_BRANCH=develop```
- Execute ```./../includes/provide-app-version.sh   ```