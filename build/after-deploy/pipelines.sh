git add pom.xml

if ls */pom.xml 1> /dev/null 2>&1; then
    git add */pom.xml
fi

if [ -f yarn.lock ]; then
    git add yarn.lock
fi