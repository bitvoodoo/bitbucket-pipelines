import os

import boto3
from botocore.config import Config

aws_region = os.getenv('AWS_REGION')

if aws_region is None:
    aws_region = 'us-east-1'
else:
    aws_region = aws_region.strip()

config = Config(
    region_name=aws_region
)

pipeline_name = os.getenv('AWS_CODEPIPELINE').strip()

print(f"Starting pipline \"{pipeline_name}\"")

client = boto3.client('codepipeline', config=config)
response = client.start_pipeline_execution(
    name=pipeline_name,
)