#!/bin/bash

SCRIPT_DIR="$(dirname "${BASH_SOURCE[0]}")"

${SCRIPT_DIR}/includes/prepare-git.sh
${SCRIPT_DIR}/includes/prepare-maven.sh
${SCRIPT_DIR}/includes/prepare-npm.sh