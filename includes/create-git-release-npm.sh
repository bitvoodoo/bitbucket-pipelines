#!/bin/bash

# handle shallow clone problems
git remote set-branches origin '*'
git fetch -v

#Update branch
git checkout -b develop origin/develop
git fetch origin develop
# fail if any command fails
set -e
# If empty use defined version
if [ -z "$APP_RELEASE_VERSION" ]; then
  APP_RELEASE_VERSION=--patch
fi
VERSION_REGEX='^(--patch|--minor|--major)$'

if [[ $APP_RELEASE_VERSION =~ $VERSION_REGEX ]]; then
 	echo "Release version: $APP_RELEASE_VERSION"
else
 	echo "ERROR:<->Unable to validate package version: '$APP_RELEASE_VERSION'"
	exit 1
fi

# The yarn release
yarn version $APP_RELEASE_VERSION --no-git-tag-version
APP_RELEASE_VERSION=$(node --eval="process.stdout.write(require('./package.json').version)")

# Start the release by creating a new release branch
git checkout -b release/$APP_RELEASE_VERSION develop

# Remove all old content
yarn clean

# deploy
yarn publish:package

git add package.json

if [ -f yarn.lock ]; then
    git add yarn.lock
fi

git commit -m "Create $APP_RELEASE_VERSION version [skip ci]"

git checkout master
git fetch origin master
# merge the version back into develop
git merge --no-ff -m "Merge release/$APP_RELEASE_VERSION into master" release/$APP_RELEASE_VERSION
#Create a Tag
git tag -a $APP_RELEASE_VERSION  -m "Create $APP_RELEASE_VERSION"
git checkout develop
git fetch origin develop
# merge the version back into develop
git merge --no-ff -m "Merge release/$APP_RELEASE_VERSION into develop" release/$APP_RELEASE_VERSION
# Removing the release branch
git branch -D release/$APP_RELEASE_VERSION
# Get back on the develop branch
git checkout develop
yarn version --prepatch --preid SNAPSHOT --no-git-tag-version
git add package.json
git commit -m "Create SNAPSHOT version [skip ci]"

git checkout master

# Push the code to Bitbucket
#git push develop
git push origin develop:develop --tags
#git push master
git push origin master:master --tags
