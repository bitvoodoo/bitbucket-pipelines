#!/bin/bash
# fail if any command fails
set -e

for i in $(find -wholename "**/build/$1/*.sh"); do # Whitespace-safe but not recursive.
    bash "$i"
done
