# set up branch names
export MASTER_BRANCH="master"
export DEVELOP_BRANCH="develop"

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $SCRIPT_DIR/provide-app-version.sh

docker login -u $DOCKER_HUB_USER -p $DOCKER_HUB_PASSWORD docker.hub.bitvoodoo.cloud
docker build -t docker.hub.bitvoodoo.cloud/bitvoodoo/$APP_ARTIFACT_ID:$APP_VERSION .
docker push docker.hub.bitvoodoo.cloud/bitvoodoo/$APP_ARTIFACT_ID:$APP_VERSION
