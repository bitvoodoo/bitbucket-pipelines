#!/bin/bash

if [[ $BITBUCKET_USERNAME ]]; then
  git config --global user.name $BITBUCKET_USERNAME

else
  echo >&2 "Failure - BITBUCKET_USER environment variable not set.  Please set this environment variable and try again."
  exit 1
fi

if [[ $BITBUCKET_USER_MAIL ]]; then
  git config --global user.email $BITBUCKET_USER_MAIL

else
  echo >&2 "Failure - BITBUCKET_USER_MAIL environment variable not set.  Please set this environment variable and try again."
  exit 1
fi
