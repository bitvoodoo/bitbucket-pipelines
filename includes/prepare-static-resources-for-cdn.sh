#!/bin/bash

# do not fail if any command fails
set +e

export MASTER_BRANCH="master"
export DEVELOP_BRANCH="develop"

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $SCRIPT_DIR/provide-app-version.sh


cd ./artifacts/target/classes/static
echo "moving static assest insight folder static/$APP_VERSION"
mkdir $APP_VERSION
mv ./* ./$APP_VERSION || true
cp -r ./$APP_VERSION ./latest