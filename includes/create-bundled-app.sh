#!/bin/bash

APP_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)

VERSION_BUNDLE="${APP_VERSION/-SNAPSHOT/""}-bundle"

echo "Clean working directory"

mvn clean --no-transfer-progress

echo "Create version with ${VERSION_BUNDLE}"

mvn versions:set -DnewVersion=$VERSION_BUNDLE -DgenerateBackupPoms=false  --no-transfer-progress
echo "done"
mvn deploy -P bundle,marketplace  --no-transfer-progress

echo "Deployment done"