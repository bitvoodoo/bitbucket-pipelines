#!/bin/bash

# fail if any command fails
set -e

python3 -m venv ./venv
source ./venv/bin/activate
python -m pip install boto3

python3 pipelines/scripts/start-codepipeline.py