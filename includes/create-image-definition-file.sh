export MASTER_BRANCH="master"
export DEVELOP_BRANCH="develop"

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $SCRIPT_DIR/provide-app-version.sh

IMAGEDEF_DIR=artifacts/imagedefinition
if [[ ! -z "$test" ]]; then
   echo "TESTING - UNSET existing entries";
   APP_ZIP_PREFIX=testing
   CONTAINER_NAME=Test
   rm -rf ${IMAGEDEF_DIR}
fi

mkdir -p ${IMAGEDEF_DIR}
mkdir -p ${IMAGEDEF_DIR}/aws
echo "[
  {
    \"name\": \"$CONTAINER_NAME-Container\",
    \"imageUri\": \"docker.hub.bitvoodoo.cloud/bitvoodoo/$APP_ARTIFACT_ID:$APP_VERSION\"
  }
]" > ${IMAGEDEF_DIR}/imagedefinitions.json

zip -rq -j ${IMAGEDEF_DIR}/aws/$APP_ZIP_PREFIX-image.zip ${IMAGEDEF_DIR}/imagedefinitions.json

echo "\nContent of created $APP_ZIP_PREFIX-image.zip:"
unzip -l ${IMAGEDEF_DIR}/aws/$APP_ZIP_PREFIX-image.zip


echo "\nContent of imagedefinitions.json:"
cat  ${IMAGEDEF_DIR}/imagedefinitions.json