#!/bin/bash

# Example Output:
# Create folder aws-output
# Create new set_env.sh
# Create new  APP_RELEASE_VERSION variable from pom.xml
# Found value in pom.xml >1.0.4-SNAPSHOT<
# Script uses APP_RELEASE_VERSION: 1.0.4 and APP_VERSION: 1.0.4-SNAPSHOT

VERSION_REGEX='^([0-9]+\.){0,2}(\*|[0-9]+)$'
ARTIFACT_DIR=artifacts/env
BRANCH="$(git rev-parse --abbrev-ref HEAD)"

# ---- Testing start ----
if [[ ! -z "$test" ]]; then
   echo "TESTING - UNSET existing entries";
   unset APP_RELEASE_VERSION
   unset APP_VERSION
   rm -rf $ARTIFACT_DIR
fi
# ---- Testing end ----


if [ ! -d "$ARTIFACT_DIR" ]; then
  echo "Create folder ${ARTIFACT_DIR}"
  mkdir -p $ARTIFACT_DIR
fi

if [[  ! -f "${ARTIFACT_DIR}/set_env.sh" ]]; then
  echo "Create new set_env.sh"
  # If empty use in pom.xml defined version
  if [ -z "$APP_RELEASE_VERSION" ]; then
    echo "Create new  APP_RELEASE_VERSION variable from pom.xml"

    if [[ "$BRANCH" = "$MASTER_BRANCH" ]]; then
        echo "It's the ${BRANCH} branch, we need to detect the APP_RELEASE_VERSION from ${DEVELOP_BRANCH}"
        # handle shallow clone problems
        git remote set-branches origin '*'
        git fetch -v

        #Update branch
        git checkout -b $DEVELOP_BRANCH origin/$DEVELOP_BRANCH
        git fetch origin $DEVELOP_BRANCH
    fi

    APP_VERSION=$(mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
    echo "Found value in pom.xml >${APP_VERSION}<"
    APP_RELEASE_VERSION=${APP_VERSION/-SNAPSHOT/""}

    if [[ "$BRANCH" = "$MASTER_BRANCH" ]]; then
        git checkout $MASTER_BRANCH
        git fetch origin $MASTER_BRANCH
    fi


  else 
      APP_VERSION=${APP_RELEASE_VERSION}
  fi

  echo "export APP_RELEASE_VERSION=$APP_RELEASE_VERSION" >> ${ARTIFACT_DIR}/set_env.sh
  echo "export APP_VERSION=$APP_VERSION" >> ${ARTIFACT_DIR}/set_env.sh
else 
  echo "Found already a set_env.sh"
fi

source ${ARTIFACT_DIR}/set_env.sh

# Validate the version and abort if it's a wrong version
  if [[ $APP_RELEASE_VERSION =~ $VERSION_REGEX ]]; then
    echo "Script uses APP_RELEASE_VERSION: $APP_RELEASE_VERSION and APP_VERSION: $APP_VERSION"
  else
    echo "ERROR: Unable to validate package version: >$APP_RELEASE_VERSION<"
    exit 1
  fi