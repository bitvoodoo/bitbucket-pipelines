#!/bin/bash

# fail if any command fails
set -e

# set up branch names
if [[ -z "${MASTER_BRANCH}" ]]; then
  export MASTER_BRANCH="master"
fi

if [[ $(git branch --show-current) != "$MASTER_BRANCH" ]] ; then
  echo "This script must be executed on $MASTER_BRANCH branch."
  exit 1;
fi

if [[ -z "${DEVELOP_BRANCH}" ]]; then
  export DEVELOP_BRANCH="develop"
fi

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
. $SCRIPT_DIR/provide-app-version.sh

# do not fail if any of the git command fails
set +e
# handle shallow clone problems
git remote set-branches origin '*'
git fetch -v


#Update branch
git checkout -b "$DEVELOP_BRANCH" "origin/$DEVELOP_BRANCH"
git fetch origin "$DEVELOP_BRANCH"
set -e

VERSION_REGEX='^([0-9]+\.){0,2}(\*|[0-9]+)$'

if [[ $APP_RELEASE_VERSION =~ $VERSION_REGEX ]]; then
 	echo "Release version: $APP_RELEASE_VERSION"
else
 	echo "ERROR:<->Unable to validate package version: '$APP_RELEASE_VERSION'"
	exit 1
fi

# Start the release by creating a new release branch
git checkout -b release/$APP_RELEASE_VERSION "$DEVELOP_BRANCH"

# The Maven release
mvn versions:set -DnewVersion=$APP_RELEASE_VERSION -DgenerateBackupPoms=false  --no-transfer-progress

# Remove all old content
mvn clean  --no-transfer-progress

if [[ -z "${MAVEN_DEPLOYMENT_PROFILE}" ]]; then
  MAVEN_DEPLOYMENT_PROFILE="production-build"
fi

# deploy
mvn deploy -P $MAVEN_DEPLOYMENT_PROFILE  --no-transfer-progress

# Tasks to be executed after deploy (which can lead to file changes)
bash $SCRIPT_DIR/execute-phase-script.sh "after-deploy"

git commit -m "Create $APP_RELEASE_VERSION version [skip ci]"

git checkout "$MASTER_BRANCH"
git fetch origin "$MASTER_BRANCH"
# merge the version back into master
git merge --no-ff -m "Merge release/$APP_RELEASE_VERSION into $MASTER_BRANCH" release/$APP_RELEASE_VERSION
#Create a Tag
git tag -a $APP_RELEASE_VERSION  -m "Create $APP_RELEASE_VERSION"
git checkout "$DEVELOP_BRANCH"
git fetch origin "$DEVELOP_BRANCH"
# merge the version back into develop
git merge --no-ff -m "Merge release/$APP_RELEASE_VERSION into $DEVELOP_BRANCH" release/$APP_RELEASE_VERSION
# Removing the release branch
git branch -D release/$APP_RELEASE_VERSION
# Get back on the develop branch
git checkout "$DEVELOP_BRANCH"
mvn versions:set -DnextSnapshot=true -DgenerateBackupPoms=false  --no-transfer-progress
git add ./\*pom.xml
git commit -m "Create snapshot version [skip ci]"

git checkout "$MASTER_BRANCH"

# Push the code to Bitbucket
#git push develop
git push origin "$DEVELOP_BRANCH:$DEVELOP_BRANCH" --tags
#git push master
git push origin "$MASTER_BRANCH:$MASTER_BRANCH" --tags
