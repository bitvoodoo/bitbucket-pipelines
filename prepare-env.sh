#!/bin/bash

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
${SCRIPT_DIR}/includes/prepare-git.sh
${SCRIPT_DIR}/includes/prepare-maven.sh
${SCRIPT_DIR}/includes/prepare-npm.sh